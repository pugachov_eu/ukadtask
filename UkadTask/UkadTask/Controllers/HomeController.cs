﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UkadTask.Models;
using UkadTask.Data;
using UkadTask.Entities;

namespace UkadTask.Controllers
{
    public class HomeController : Controller
    {
        TaskRepository taskRepo;
        public HomeController()
        {
            taskRepo = new TaskRepository();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RequestList()
        {
            return View(taskRepo.GetSiteList().OrderByDescending(item =>item.TimeOfRequest));
        }
        [HttpPost]
        public ActionResult Performance(string url)
        {
          //url ="http://stackoverflow.com/";
            url = "http://www.mysitemapgenerator.com/";
            string siteMapPath = GetSiteMapPath(url);
            //проверка на полноту пути
            List<string> linkToRequst = GetSiteMap(siteMapPath.Contains(url) ? siteMapPath : $"{url}{siteMapPath}");

            Site site = new Site();
            var sourceList = SendRequsts(linkToRequst, site);

            //проверить на соответствие
            double doubleAverageTicks = sourceList.Average(timeSpan => timeSpan.Time.Ticks);
            long longAverageTicks = Convert.ToInt64(doubleAverageTicks);

           
            site.SitePath = url;
            site.TimeOfRequest = DateTime.Now;
            site.MaxTime = sourceList.Max(item => item.Time);
            site.MinTime = sourceList.Min(item => item.Time);
            site.AverageTime = new TimeSpan(longAverageTicks);
            taskRepo.CreateSite(site);
           
            taskRepo.CreateResults(sourceList);
            taskRepo.Save();
            return View(new RequestTimeModel
            {
                Results = sourceList.OrderByDescending(item => item.Time).ToList(),
                AveregeTime = site.AverageTime,
                URL = site.SitePath,
                MaxValue = site.MaxTime,
                MinValue = site.MinTime
            });
        }

        public List<Result> SendRequsts(List<string> list, Site site)
        {

            List<Result> results = new List<Result>();
            foreach (var t in list)
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(t);
                    Stopwatch timer = new Stopwatch();
                    timer.Start();
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    timer.Stop();
                    results.Add(new Result { Page = t, Time = timer.Elapsed, Site = site });
                    System.Threading.Thread.Sleep(1000);
                }
                catch (WebException)
                {
                    results.Add(new Result { Page = t, Time = TimeSpan.MinValue, Site = site });
                }
            }

            return results;
        }
        public List<string> GetSiteMap(string url)
        {
            // url = "http://www.mysitemapgenerator.com/sitemap.xml";
            var webRequest = WebRequest.Create(url);
            using (var response = webRequest.GetResponse())
            using (var content = response.GetResponseStream())
            using (var reader = new StreamReader(content))
            {
                var strContent = reader.ReadToEnd();
                return Regex.Matches(strContent, @"(?<=\<loc\>)(.*?)(?=<\/loc\>)").Cast<Match>().Select(mm => mm.Value.ToString()).ToList();
            }
        }
        public string GetSiteMapPath(string url)
        {
            string path = null;
            try
            {
                var webRequest = WebRequest.Create($"{url}robots.txt");
                using (var response = webRequest.GetResponse())
                using (var content = response.GetResponseStream())
                using (var reader = new StreamReader(content))
                {
                    var strContent = reader.ReadToEnd();
                    Regex re = new Regex(@"Sitemap:(.*)");
                    path = re.Match(strContent).Value;
                    path = path.Substring(path.IndexOf(':') + 2);
                }
            }
            catch (WebException)
            {
                return "/sitemap.xml";//не смогли получить robots.txt, используем по умолчанию
            }
            return path;
        }
    }
}