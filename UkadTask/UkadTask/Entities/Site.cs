﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UkadTask.Entities
{
    public class Site
    {
        public int SiteId { get; set; }
        [Display(Name = "URL")]
        public string SitePath { get; set; }
        [Display(Name = "Average response time")]
        public TimeSpan AverageTime { get; set; }
        [Display(Name = "Min response time")]
        public TimeSpan MinTime { get; set; }
        [Display(Name = "Max response time")]
        public TimeSpan MaxTime { get; set; }
        [Display(Name = "Date")]
        public DateTime TimeOfRequest  { get; set; }

        public ICollection<Result> Results { get; set; }
        public Site()
        {
            Results = new List<Result>();
        }
    }
}