﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace UkadTask.Entities
{
    public class TaskContext: DbContext
    {
        public TaskContext(): base("TaskContext")
        { }

        public DbSet<Site> Sites { get; set; }
        public DbSet<Result> Results { get; set; }
    }
}