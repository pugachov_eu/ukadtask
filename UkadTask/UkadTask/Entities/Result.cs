﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UkadTask.Entities
{
    public class Result
    {
        public int ResultId { get; set; }
        [Display(Name = "Page")]
        public string Page { get; set; }
        [Display(Name = "Response time")]
        public TimeSpan Time { get; set; }

        public int SiteId { get; set; }
        public Site Site { get; set; }
    }
}