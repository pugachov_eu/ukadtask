﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UkadTask.Entities;
using UkadTask.Interfaces;

namespace UkadTask.Data
{
    public class TaskRepository : ITaskRepository
    {
        private TaskContext db;
        public TaskRepository()
        {
            this.db = new TaskContext();
        }

        public void CreateResult(Result item)
        {
            db.Results.Add(item);
        }

        public void CreateResults(List<Result> items)
        {
            db.Results.AddRange(items);
        }

        public void CreateSite(Site item)
        {
            db.Sites.Add(item);
        }

        public void DeleteResult(int id)
        {
            Result res = db.Results.Find(id);
            if (res != null)
            {
                db.Results.Remove(res);
            }
        }

        public void DeleteSite(int id)
        {
            Site site = db.Sites.Find(id);
            if (site != null)
            {
                db.Sites.Remove(site);
            }
        } 

        public IEnumerable<Result> GetResultListBySiteId(int id)
        {
            return db.Results.Where(r=> r.SiteId==id).ToList();
        }

        public Site GetSite(int id)
        {
            return db.Sites.Find(id);
        }

        public IEnumerable<Site> GetSiteList()
        {
            return db.Sites.ToList<Site>();
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public void UpdateSite(Site item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }
    }
}