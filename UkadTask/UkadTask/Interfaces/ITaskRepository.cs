﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UkadTask.Entities;

namespace UkadTask.Interfaces
{
    interface ITaskRepository:IDisposable
    {
        IEnumerable<Site> GetSiteList();
        Site GetSite(int id);
        void CreateSite(Site item);
        void UpdateSite(Site item);
        void DeleteSite(int id);
        void Save();

        IEnumerable<Result> GetResultListBySiteId(int id);
        void CreateResult(Result item);
        void DeleteResult(int id);


    }
}
