﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UkadTask.Entities;

namespace UkadTask.Models
{
    public class RequestTimeModel
    {
        public string URL { get; set; }
        public TimeSpan AveregeTime { get; set; }
        public TimeSpan MinValue { get; set; }
        public TimeSpan MaxValue { get; set; }
        public List<Result> Results { get; set; }
    }
}