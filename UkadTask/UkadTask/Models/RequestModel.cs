﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UkadTask.Models
{
    public class RequestModel
    {
       public string Page { get; set; }
       public TimeSpan Time { get; set; }
    }
}